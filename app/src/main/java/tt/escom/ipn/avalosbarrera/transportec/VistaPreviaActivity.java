package tt.escom.ipn.avalosbarrera.transportec;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by laavalos on 11/18/2018.
 */

public class VistaPreviaActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Marker marcadorOrigen, marcadorDestino;
    int idRuta;
    LatLng origen, destino;
    LatLng punto;
    int numPunto;

    ArrayList<LatLng> puntosEnRuta = new ArrayList<LatLng>();
    ArrayList<Integer> numPuntos = new ArrayList<Integer>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista_previa);
        Intent i = getIntent();
        origen = new LatLng(i.getDoubleExtra("latitudOrigen", 0.0), i.getDoubleExtra("longitudOrigen", 0.0));
        destino = new LatLng(i.getDoubleExtra("latitudDestino", 0.0), i.getDoubleExtra("longitudDestino", 0.0));
        idRuta = i.getIntExtra("idRuta", 0);

        obtenerPuntosRuta(idRuta);
        try {
            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.fragmentVistaPrevia);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Button listo = (Button) findViewById(R.id.button21);
        listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        configurarMapa(mMap);
    }

    private void configurarMapa(GoogleMap map){

        if (marcadorOrigen != null) marcadorOrigen.remove();
        {
            marcadorOrigen = map.addMarker(new MarkerOptions().position(origen).title("Origen").draggable(true));
        }
        if (marcadorDestino != null) marcadorDestino.remove();
        {
            marcadorDestino = map.addMarker(new MarkerOptions().position(destino).title("Destino").draggable(true));
        }



    }

    private void obtenerPuntosRuta(int idRuta){

        String urlFinal = "https://api-transportec.herokuapp.com/public/detalle_ruta/" + idRuta;

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, urlFinal, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray puntosIda = response.optJSONObject("data").optJSONObject("puntos").optJSONArray("Ida");
                JSONArray puntosRegreso = response.optJSONObject("data").optJSONObject("puntos").optJSONArray("Regreso");
                JSONArray puntosUnico = response.optJSONObject("data").optJSONObject("puntos").optJSONArray("Unico");

                if(puntosUnico != null){
                    for(int y=0; y < puntosUnico.length(); y++){
                        JSONObject detallePunto = (JSONObject) puntosUnico.opt(y);
                        punto = new LatLng(detallePunto.optDouble("latitud"), detallePunto.optDouble("longitud"));
                        numPunto = detallePunto.optInt("num_punto");
                        puntosEnRuta.add(punto);
                        numPuntos.add(numPunto);
                        Log.d("OK", "Good");
                    }
                    PolylineOptions po = new PolylineOptions();
                    po.addAll(puntosEnRuta).width(8f).color(Color.GREEN);

                    Polyline polylineRuta = mMap.addPolyline(po);

                    CameraUpdate ubicacion = CameraUpdateFactory.newLatLngZoom(origen, 13);
                    mMap.animateCamera(ubicacion);
                }
                else{
                    if(puntosIda != null){
                        for(int y=0; y < puntosIda.length(); y++) {
                            JSONObject detallePunto = (JSONObject) puntosIda.opt(y);
                            punto = new LatLng(detallePunto.optDouble("latitud"), detallePunto.optDouble("longitud"));
                            numPunto = detallePunto.optInt("num_punto");
                            puntosEnRuta.add(punto);
                            numPuntos.add(numPunto);
                            Log.d("OK", "Good");
                        }
                        PolylineOptions po = new PolylineOptions();
                        po.addAll(puntosEnRuta).width(8f).color(Color.GREEN);

                        Polyline polylineRuta = mMap.addPolyline(po);

                        CameraUpdate ubicacion = CameraUpdateFactory.newLatLngZoom(origen, 13);
                        mMap.animateCamera(ubicacion);
                    }
               }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);
    }

}
