package tt.escom.ipn.avalosbarrera.transportec;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by laavalos on 11/17/2018.
 */

public class ElegirRuta2Activity extends AppCompatActivity {

    Spinner spinner;
    ArrayAdapter adp;
    LatLng origen, destino;
    int idRuta;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elegir_ruta_2);

        ArrayList<Integer> rutas = new ArrayList<Integer>();
        Intent i = getIntent();
        rutas = i.getIntegerArrayListExtra("rutas");
        origen = new LatLng(i.getDoubleExtra("latitudOrigen", 0.0), i.getDoubleExtra("longitudOrigen", 0.0));
        destino = new LatLng(i.getDoubleExtra("latitudDestino", 0.0), i.getDoubleExtra("longitudDestino", 0.0));

        spinner = (Spinner) findViewById(R.id.spinner);
        findViewById(R.id.button17).setVisibility(View.INVISIBLE);
        findViewById(R.id.button19).setVisibility(View.INVISIBLE);
        mostrarRutas(rutas);

        Button atras = (Button) findViewById(R.id.button20);
        Button vistaPrevia = (Button) findViewById(R.id.button17);
        Button iniciar = (Button) findViewById(R.id.button19);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        vistaPrevia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(ElegirRuta2Activity.this, VistaPreviaActivity.class);
                intent.putExtra("idRuta", idRuta);
                intent.putExtra("latitudOrigen", origen.latitude);
                intent.putExtra("longitudOrigen", origen.longitude);
                intent.putExtra("latitudDestino", destino.latitude);
                intent.putExtra("longitudDestino", destino.longitude);
                startActivity(intent);
            }
        });

        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(ElegirRuta2Activity.this, RecorridoActivity.class);
                intent.putExtra("idRuta", idRuta);
                intent.putExtra("latitudOrigen", origen.latitude);
                intent.putExtra("longitudOrigen", origen.longitude);
                intent.putExtra("latitudDestino", destino.latitude);
                intent.putExtra("longitudDestino", destino.longitude);
                AlertDialog alerta = new AlertDialog.Builder(ElegirRuta2Activity.this).create();
                alerta.setTitle("Confirmación");
                alerta.setMessage("Desea Iniciar el recorrido");
                alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        startActivity(intent);
                    }
                });
                alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alerta.show();
            }
        });


    }

    private void mostrarRutas(ArrayList<Integer> rutas){

        String url = "https://api-transportec.herokuapp.com/public/detalle_ruta/";
        String finalUrl = "";
        final ArrayList<String> listaRutas = new ArrayList<String>();
        final ArrayList<Integer> idRutas = new ArrayList<Integer>();


        for(int x=0; x < rutas.size(); x++){
            finalUrl = url + rutas.get(x);
            RequestQueue queue = Volley.newRequestQueue(this);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, finalUrl, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String nombre = response.optJSONObject("data").optJSONArray("detalle").optJSONObject(0).optString("nombre_ruta");
                    String transporte = response.optJSONObject("data").optJSONArray("detalle").optJSONObject(0).optString("transporte");
                    int idRuta = response.optJSONObject("data").optJSONArray("detalle").optJSONObject(0).optInt("id_ruta");
                    idRutas.add(idRuta);
                    listaRutas.add(" " + transporte + " " + nombre);
                    adp.notifyDataSetChanged();
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            queue.add(request);

        }
        adp = new ArrayAdapter(ElegirRuta2Activity.this, android.R.layout.simple_list_item_1, listaRutas);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adp);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                findViewById(R.id.button17).setVisibility(View.VISIBLE);

                idRuta = idRutas.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                findViewById(R.id.button17).setVisibility(View.VISIBLE);
                findViewById(R.id.button19).setVisibility(View.VISIBLE);
            }
        });
    }



}
