package tt.escom.ipn.avalosbarrera.transportec;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by laavalos on 10/29/2018.
 */

public class PorMapaActivity extends AppCompatActivity implements OnMapReadyCallback {

    private final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 100;
    private final int PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 101;
    private GoogleMap mMap;
    private Marker marcador;
    double lat = 0.0;
    double lng = 0.0;
    double latOrigen, lngOrigen, latDestino, lngDestino;
    private boolean isOrigen, isModificar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_por_mapa);
        findViewById(R.id.button7).setVisibility(View.INVISIBLE);
        isOrigen = getIntent().getBooleanExtra("isOrigen", true);
        isModificar = getIntent().getBooleanExtra("isModificar", false);
        latOrigen = getIntent().getDoubleExtra("latitudOrigen", 0.0);
        lngOrigen = getIntent().getDoubleExtra("longitudOrigen", 0.0);
        latDestino = getIntent().getDoubleExtra("latitudDestino", 0.0);
        lngDestino = getIntent().getDoubleExtra("longitudDestino", 0.0);

        try {
            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.fragmentmapPorMapa);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        accionButton();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        miUbicacion();
        agregarMarcador(lat,lng);
        agregarColonia(mMap);
    }

    private void agregarColonia(GoogleMap map){
        //Polygon de Agricola Oriental
        Polygon polygonAO = map.addPolygon(new PolygonOptions()
            .add(new LatLng(19.408485,-99.077111),
                    new LatLng(19.408547,-99.077310),
                    new LatLng(19.408444,-99.077093),
                    new LatLng(19.399564,-99.082895),
                    new LatLng(19.399564,-99.082683),
                    new LatLng(19.392581,-99.084022),
                    new LatLng(19.392479,-99.083626),
                    new LatLng(19.383528,-99.066385),
                    new LatLng(19.382598,-99.060856),
                    new LatLng(19.383143,-99.060843),
                    new LatLng(19.394266,-99.058973),
                    new LatLng(19.398228,-99.058570),
                    new LatLng(19.408485,-99.077111)).strokeColor(Color.RED).strokeWidth(5f));


        //Polygon de Agricola Pantitlan
        Polygon polygonAP = map.addPolygon(new PolygonOptions()
            .add(new LatLng(19.422144,-99.0555782),
                new LatLng(19.422230,-99.056035),
                new LatLng(19.421943,-99.056217),
                new LatLng(19.421724,-99.056379),
                new LatLng(19.421380,-99.056795),
                new LatLng(19.421179,-99.057139),
                new LatLng(19.420893,-99.057909),
                new LatLng(19.420816,-99.067985),
                new LatLng(19.420769,-99.068299),
                new LatLng(19.420530,-99.068997),
                new LatLng(19.420071,-99.069747),
                new LatLng(19.419833,-99.069949),
                new LatLng(19.408806,-99.077110),
                new LatLng(19.398224,-99.058554),
                new LatLng(19.398302,-99.058560),
                new LatLng(19.400741,-99.058265),
                new LatLng(19.400725,-99.058130),
                new LatLng(19.421965,-99.055832),
                new LatLng(19.422144,-99.0555782)).strokeColor(Color.BLUE).strokeWidth(5f));
        CameraUpdate ubicacion = CameraUpdateFactory.newLatLngZoom(new LatLng(19.403925, -99.067750), 13);
        map.animateCamera(ubicacion);

    }

    private void agregarMarcador(double lat, double lng) {
        LatLng coordenadas = new LatLng(lat, lng);

        if (marcador != null) marcador.remove();
        {
            marcador = mMap.addMarker(new MarkerOptions().position(coordenadas).title("Ubicacion Actual").draggable(true));
        }
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                marcador.setPosition(latLng);
                if(validarDireccion(marcador.getPosition().latitude, marcador.getPosition().longitude)){
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }

            }
        });
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                marcador.setPosition(marker.getPosition());
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
            }
        });

    }



    private void actualizarUbicacion(Location location) {
        if (location != null) {
            lat = location.getLatitude();
            lng = location.getLongitude();
        }

    }

    LocationListener locListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            actualizarUbicacion(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

    private void miUbicacion() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        actualizarUbicacion(location);
    }

    private void accionButton(){
        Button aceptar = (Button) findViewById(R.id.button7);
        Button atras = (Button) findViewById(R.id.button6);

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng ubicacion = marcador.getPosition();
                if(validarDireccion(ubicacion.latitude, ubicacion.longitude)) {
                    if (isOrigen) {
                        if(!isModificar) {
                            latOrigen = ubicacion.latitude;
                            lngOrigen = ubicacion.longitude;
                            final Intent intent = new Intent(PorMapaActivity.this, DestinoActivity.class);
                            intent.putExtra("latitudOrigen", latOrigen);
                            intent.putExtra("longitudOrigen", lngOrigen);
                            AlertDialog alerta = new AlertDialog.Builder(PorMapaActivity.this).create();
                            alerta.setTitle("Confirmación");
                            alerta.setMessage("Desea seleccionar esta ubicacion como Origen");
                            alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(intent);
                                }
                            });
                            alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            alerta.show();
                        }
                        else{
                            latOrigen = ubicacion.latitude;
                            lngOrigen = ubicacion.longitude;
                            final Intent intent = new Intent(PorMapaActivity.this, ElegirRutaActivity.class);
                            intent.putExtra("latitudOrigen", latOrigen);
                            intent.putExtra("longitudOrigen", lngOrigen);
                            intent.putExtra("latitudDestino", latDestino);
                            intent.putExtra("longitudDestino", lngDestino);
                            AlertDialog alerta = new AlertDialog.Builder(PorMapaActivity.this).create();
                            alerta.setTitle("Confirmación");
                            alerta.setMessage("Desea seleccionar esta ubicacion como Origen");
                            alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(intent);
                                }
                            });
                            alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            alerta.show();
                        }
                    }else {
                        if(isOrigen){
                            latOrigen = ubicacion.latitude;
                            lngOrigen = ubicacion.longitude;
                            final Intent intent = new Intent(PorMapaActivity.this, ElegirRutaActivity.class);
                            intent.putExtra("latitudOrigen", latOrigen);
                            intent.putExtra("longitudOrigen", lngOrigen);
                            intent.putExtra("latitudDestino", latDestino);
                            intent.putExtra("longitudDestino", lngDestino);
                            AlertDialog alerta = new AlertDialog.Builder(PorMapaActivity.this).create();
                            alerta.setTitle("Confirmación");
                            alerta.setMessage("Desea seleccionar esta ubicacion como Origen");
                            alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(intent);
                                }
                            });
                            alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            alerta.show();
                        }
                        else {
                            latDestino = ubicacion.latitude;
                            lngDestino = ubicacion.longitude;
                            final Intent intent = new Intent(PorMapaActivity.this, ElegirRutaActivity.class);
                            intent.putExtra("latitudOrigen", latOrigen);
                            intent.putExtra("longitudOrigen", lngOrigen);
                            intent.putExtra("latitudDestino", latDestino);
                            intent.putExtra("longitudDestino", lngDestino);
                            AlertDialog alerta = new AlertDialog.Builder(PorMapaActivity.this).create();
                            alerta.setTitle("Confirmación");
                            alerta.setMessage("Desea seleccionar esta ubicacion como Destino");
                            alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(intent);
                                }
                            });
                            alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            alerta.show();
                        }
                    }
                }
            }
        });

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private boolean validarDireccion(double lat, double lng){
        //new LatLngBounds(new LatLng(19.382598, -99.084022), new LatLng(19.422230, -99.0555782));
        LatLngBounds areaColonias = new LatLngBounds(new LatLng(19.382598, -99.084022), new LatLng(19.422230, -99.0555782));
        LatLng ubicacion = new LatLng(lat, lng);
        boolean b = areaColonias.contains(ubicacion);
        if(!areaColonias.contains(ubicacion)){
            AlertDialog alerta = new AlertDialog.Builder(PorMapaActivity.this).create();
            alerta.setTitle("Advertencia");
            alerta.setMessage("La Ubicación seleccionada no es válida. Selecciona una ubicación válida");
            alerta.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    findViewById(R.id.button7).setVisibility(View.INVISIBLE);
                    dialogInterface.dismiss();
                }
            });
            alerta.show();
        }
        else{
            findViewById(R.id.button7).setVisibility(View.VISIBLE);
        }
        return b;
    }
}
