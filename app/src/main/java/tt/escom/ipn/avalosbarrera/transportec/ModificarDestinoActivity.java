package tt.escom.ipn.avalosbarrera.transportec;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by laavalos on 11/5/2018.
 */

public class ModificarDestinoActivity extends AppCompatActivity {

    double latOrigen, lngOrigen, latDestino, lngDestino;
    boolean isModificar = true;
    private String urlUbicacion = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
    private String keyAPI = "&key=AIzaSyCdq-SYOMucNaY7CVYoagG-pY7ppgoihP0";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_destino);
        Intent i = getIntent();
        latOrigen = i.getDoubleExtra("latitudOrigen", 0.0);
        lngOrigen = i.getDoubleExtra("longitudOrigen", 0.0);
        latDestino = i.getDoubleExtra("latitudDestino", 0.0);
        lngDestino = i.getDoubleExtra("longitudDestino", 0.0);


        try {
            pintarDatos(latOrigen, lngOrigen);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Button modificar = (Button) findViewById(R.id.button11);
        Button cancelar = (Button) findViewById(R.id.button8);
        final RadioButton rbUbicacion = (RadioButton) findViewById(R.id.radioButton4);
        final RadioButton rbMapa = (RadioButton) findViewById(R.id.radioButton7);
        modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rbMapa.isChecked()) {
                    Intent i = new Intent(ModificarDestinoActivity.this, PorMapaActivity.class);
                    i.putExtra("latitudOrigen", latOrigen);
                    i.putExtra("longitudOrigen", lngOrigen);
                    i.putExtra("isOrigen", false);
                    i.putExtra("isModificar", true);
                    startActivity(i);

                }
                else if(rbUbicacion.isChecked()) {
                    Intent i = new Intent(ModificarDestinoActivity.this, PorDireccionActivity.class);
                    i.putExtra("latitudOrigen", latOrigen);
                    i.putExtra("longitudOrigen", lngOrigen);
                    i.putExtra("isOrigen", false);
                    i.putExtra("isModificar", true);
                    startActivity(i);

                }
            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void pintarDatos(double lat, double lng) throws IOException, JSONException {
        try {
            obtenerDireccion(lat, lng);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void obtenerDireccion(double lat, double lng) throws IOException, JSONException {
        String finalUrl = urlUbicacion+lat+","+lng+keyAPI;

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest
                (Request.Method.GET, finalUrl, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            // Se obtiene la primera direccion del json que regresa la api de google maps
                            JSONArray direccion1 = response.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
                            String calle = direccion1.getJSONObject(0).getString("long_name") + " " + direccion1.getJSONObject(1).getString("long_name") + " " + direccion1.getJSONObject(2).getString("long_name");
                            String colonia = direccion1.getJSONObject(3).getString("long_name") + " " + direccion1.getJSONObject(4).getString("long_name") + " " + direccion1.getJSONObject(5).getString("long_name");
                            // Se pasan los valores obtenidos del Json a los labels de la vista
                            TextView tvCalle = (TextView) findViewById(R.id.textView20);
                            tvCalle.setText(calle);
                            TextView tvColonia = (TextView) findViewById(R.id.textView19);
                            tvColonia.setText(colonia);
                        }
                        catch (JSONException e){
                            e.printStackTrace();
                        }
                        // TODO Auto-generated method stub
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                    }
                });
        queue.add(request);
    }

}
