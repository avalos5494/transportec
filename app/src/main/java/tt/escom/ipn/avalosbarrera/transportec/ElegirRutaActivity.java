package tt.escom.ipn.avalosbarrera.transportec;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by laavalos on 11/5/2018.
 */

public class ElegirRutaActivity extends AppCompatActivity {

    double latOrigen, lngOrigen, latDestino, lngDestino;
    private String urlUbicacion = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
    private String keyAPI = "&key=AIzaSyCdq-SYOMucNaY7CVYoagG-pY7ppgoihP0";
    public ArrayList<Integer> rutasResult = new ArrayList<Integer>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elegir_ruta);

        Intent i = getIntent();
        latOrigen = i.getDoubleExtra("latitudOrigen", 0.0);
        lngOrigen = i.getDoubleExtra("longitudOrigen", 0.0);
        latDestino = i.getDoubleExtra("latitudDestino", 0.0);
        lngDestino = i.getDoubleExtra("longitudDestino", 0.0);
        final TextView tvCalleOrigen = (TextView) findViewById(R.id.textView30);
        final TextView tvColoniaOrigen = (TextView) findViewById(R.id.textView31);
        final TextView tvCalleDestino = (TextView) findViewById(R.id.textView32);
        final TextView tvColoniaDestino = (TextView) findViewById(R.id.textView33);

        try {
            obtenerDireccion(latOrigen, lngOrigen, tvCalleOrigen, tvColoniaOrigen);
            obtenerDireccion(latDestino, lngDestino, tvCalleDestino, tvColoniaDestino);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        accionBotones();
        accionRutas();
    }

    private void obtenerDireccion(double lat, double lng, final TextView tvCalle, final TextView tvColonia) throws IOException, JSONException {
        String finalUrl = urlUbicacion+lat+","+lng+keyAPI;

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest
                (Request.Method.GET, finalUrl, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            // Se obtiene la primera direccion del json que regresa la api de google maps
                            JSONArray direccion1 = response.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
                            String calle = direccion1.getJSONObject(0).getString("long_name") + " " + direccion1.getJSONObject(1).getString("long_name") + " " + direccion1.getJSONObject(2).getString("long_name");
                            String colonia = direccion1.getJSONObject(2).getString("long_name") + " " + direccion1.getJSONObject(4).getString("long_name") + " " + direccion1.getJSONObject(5).getString("long_name") + " " + direccion1.getJSONObject(6).getString("long_name");
                            // Se pasan los valores obtenidos del Json a los labels de la vista
                            tvCalle.setText(calle);
                            tvColonia.setText(colonia);
                        }
                        catch (JSONException e){
                            e.printStackTrace();
                        }
                        // TODO Auto-generated method stub
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                    }
                });
        queue.add(request);
    }

    public void accionBotones(){

        Button modificarOrigen = (Button) findViewById(R.id.button14);
        Button modificarDestino = (Button) findViewById(R.id.button18);
        Button Atras = (Button) findViewById(R.id.button15);

        modificarOrigen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ElegirRutaActivity.this, ModificarOrigenActivity.class);
                i.putExtra("latitudDestino", latDestino);
                i.putExtra("longitudDestino", lngDestino);
                i.putExtra("latitudOrigen", latOrigen);
                i.putExtra("longitudOrigen", lngOrigen);
                i.putExtra("isModificar", true);
                startActivity(i);
            }
        });

        modificarDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ElegirRutaActivity.this, ModificarDestinoActivity.class);
                i.putExtra("latitudOrigen", latOrigen);
                i.putExtra("longitudOrigen", lngOrigen);
                i.putExtra("latitudDestino", latDestino);
                i.putExtra("longitudDestino", lngDestino);
                i.putExtra("isModificar", true);
                startActivity(i);
            }
        });

        Atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void accionRutas(){

        Button buscar = (Button) findViewById(R.id.button16);
        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buscarRutas(latOrigen, lngOrigen, latDestino, lngDestino);

            }
        });

    }

    private void buscarRutas(double latOrigen, double lngOrigen, double latDestino, double lngDestino){

        final boolean[] isEmpty = {true};
        final LatLng origen = new LatLng(latOrigen, lngOrigen);
        final LatLng destino = new LatLng(latDestino, lngDestino);
        final JSONObject ubicaciones = new JSONObject();
        String url = "https://api-transportec.herokuapp.com/public/obtener_rutas";

        try{
            ubicaciones.put("latitudOrigen", origen.latitude); //latitudOrigen en el json es equivalente a la latitud minima en la busqueda en la bd
            ubicaciones.put("longitudOrigen", origen.longitude); //latitudOrigen en el json es equivalente a la latitud minima en la busqueda en la bd
            ubicaciones.put("latitudDestino", destino.latitude); //latitudOrigen en el json es equivalente a la latitud minima en la busqueda en la bd
            ubicaciones.put("longitudDestino", destino.longitude); //latitudOrigen en el json es equivalente a la latitud minima en la busqueda en la bd
            final int[] distancia = {50};
            final ProgressDialog pg = new ProgressDialog(this);
            pg.setTitle("Cargando");
            pg.setMessage("Buscando Rutas...");
            pg.setCancelable(false);
            pg.show();

            ubicaciones.put("distancia", distancia[0]);
            RequestQueue queue = Volley.newRequestQueue(this);
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, ubicaciones, new Response.Listener<JSONObject>() {
                    @Override
                  public void onResponse(JSONObject response) {
                        boolean success = response.optBoolean("success");
                        if (success == true) {
                            JSONArray rutas = response.optJSONArray("rutas");
                            if(rutas.length() > 0) {
                                for (int x = 0; x < rutas.length(); x++) {
                                    rutasResult.add(rutas.optInt(x));
                                }
                                pg.dismiss();
                                Intent i = new Intent(ElegirRutaActivity.this, ElegirRuta2Activity.class);
                                i.putExtra("rutas", rutasResult);
                                i.putExtra("latitudOrigen", origen.latitude);
                                i.putExtra("longitudOrigen", origen.longitude);
                                i.putExtra("latitudDestino", destino.latitude);
                                i.putExtra("longitudDestino", destino.longitude);
                                startActivity(i);
                                isEmpty[0] = false;
                            }
                            else{
                                isEmpty[0] = true;
                                distancia[0] += 50;
                            }
                        }
                        else{
                            Log.d("MESSAGE", "Error al obtener las rutas de la BD");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR", error.toString());
                        pg.dismiss();
                        //Toast.
                    }
                });
                queue.add(request);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
