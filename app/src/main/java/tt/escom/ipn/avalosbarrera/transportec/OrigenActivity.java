package tt.escom.ipn.avalosbarrera.transportec;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by laavalos on 5/22/2018.
 */

public class OrigenActivity extends FragmentActivity implements OnMapReadyCallback {

    private final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 100;
    private final int PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 101;
    private GoogleMap mMap;
    private Marker marcador;
    double lat, lng;
    private Geocoder geocoder;
    List<Address> addresses;
    private String urlUbicacion = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
    private String keyAPI = "&key=AIzaSyCdq-SYOMucNaY7CVYoagG-pY7ppgoihP0";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origen);
        try {
            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.fragmentmap);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        eleccionOrigen();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        miUbicacion();
        if(validarDireccion(lat, lng)) {
            agregarMarcador(lat, lng);
            agregarColonia(mMap);
            try {
                obtenerDireccion(lat, lng);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

     private void agregarMarcador(double lat, double lng) {
        LatLng coordenadas = new LatLng(lat, lng);
        CameraUpdate ubicacion = CameraUpdateFactory.newLatLngZoom(coordenadas, 14);
        if (marcador != null) marcador.remove();
        {
            marcador = mMap.addMarker(new MarkerOptions().position(coordenadas).title("Ubicacion Actual"));
            mMap.animateCamera(ubicacion);
        }
    }

    private void actualizarUbicacion(Location location) {
        if (location != null) {
            lat = location.getLatitude();
            lng = location.getLongitude();
        }

    }

    LocationListener locListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            actualizarUbicacion(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

    private void miUbicacion() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        actualizarUbicacion(location);
    }

    private void eleccionOrigen(){
        final RadioButton rbUbicacionMapa = (RadioButton) findViewById(R.id.radioButton);
        final RadioButton rbUbicacionDireccion = (RadioButton) findViewById(R.id.radioButton2);
        final RadioButton rbUbicacionActual = (RadioButton) findViewById(R.id.radioButton3);
        Button aceptar = (Button) findViewById(R.id.button3);

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rbUbicacionMapa.isChecked()){
                    startActivity(new Intent(OrigenActivity.this, PorMapaActivity.class));
                }
                else if(rbUbicacionDireccion.isChecked()){
                    startActivity(new Intent(OrigenActivity.this, PorDireccionActivity.class));
                }
                else if(rbUbicacionActual.isChecked()){
                    final Intent intent = new Intent(OrigenActivity.this, DestinoActivity.class);
                    intent.putExtra("latitudOrigen", lat);
                    intent.putExtra("longitudOrigen", lng);
                    AlertDialog alerta = new AlertDialog.Builder(OrigenActivity.this).create();
                    alerta.setTitle("Confirmación");
                    alerta.setMessage("Desea seleccionar esta ubicacion como Origen");
                    alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            startActivity(intent);
                        }
                    });
                    alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    alerta.show();
                }
            }
        });
    }

    private boolean validarDireccion(double lat, double lng){
        //new LatLngBounds(new LatLng(19.382598, -99.084022), new LatLng(19.422230, -99.0555782)) Bound en los limites de las colonias
        LatLngBounds areaColonias = new LatLngBounds(new LatLng(19.382598, -99.084022), new LatLng(19.422230, -99.0555782));
        LatLng ubicacion = new LatLng(lat, lng);
        boolean b = areaColonias.contains(ubicacion);
        if(!areaColonias.contains(ubicacion)){
            AlertDialog alerta = new AlertDialog.Builder(OrigenActivity.this).create();
            alerta.setTitle("Advertencia");
            alerta.setMessage("La Ubicación actual del dispositivo no se encuentra en el area valida");
            alerta.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    findViewById(R.id.radioButton3).setVisibility(View.INVISIBLE);
                    findViewById(R.id.fragmentmap).setVisibility(View.INVISIBLE);
                    TextView temp = (TextView) findViewById(R.id.textView3);
                    temp.setText("Seleccione una opción para establecer una ubicación origen");
                    dialogInterface.dismiss();
                }
            });
            alerta.show();

        }
        return b;
    }

    private void obtenerDireccion(double lat, double lng) throws IOException, JSONException {
        String finalUrl = urlUbicacion+lat+","+lng+keyAPI;

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest
                (Request.Method.GET, finalUrl, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            // Se obtiene la primera direccion del json que regresa la api de google maps
                            JSONArray direccion1 = response.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
                            String calle = direccion1.getJSONObject(0).getString("long_name") + " " + direccion1.getJSONObject(1).getString("long_name");
                            String colonia = direccion1.getJSONObject(2).getString("long_name") + " " + direccion1.getJSONObject(3).getString("long_name");
                            String cp = direccion1.getJSONObject(5).getString("long_name") + " " + direccion1.getJSONObject(6).getString("long_name");
                            // Se pasan los valores obtenidos del Json a los labels de la vista
                            TextView tvCalle = (TextView) findViewById(R.id.textView4);
                            tvCalle.setText(calle);
                            TextView tvColonia = (TextView) findViewById(R.id.textView5);
                            tvColonia.setText(colonia);
                            TextView tvCp = (TextView) findViewById(R.id.textView6);
                            tvCp.setText(cp);

                        }
                        catch (JSONException e){
                            e.printStackTrace();
                        }
                        // TODO Auto-generated method stub
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                    }
                });
        queue.add(request);
    }

    private void agregarColonia(GoogleMap map){
        //Polygon de Agricola Oriental
        Polygon polygonAO = map.addPolygon(new PolygonOptions()
                .add(new LatLng(19.408485,-99.077111),
                        new LatLng(19.408547,-99.077310),
                        new LatLng(19.408444,-99.077093),
                        new LatLng(19.399564,-99.082895),
                        new LatLng(19.399564,-99.082683),
                        new LatLng(19.392581,-99.084022),
                        new LatLng(19.392479,-99.083626),
                        new LatLng(19.383528,-99.066385),
                        new LatLng(19.382598,-99.060856),
                        new LatLng(19.383143,-99.060843),
                        new LatLng(19.394266,-99.058973),
                        new LatLng(19.398228,-99.058570),
                        new LatLng(19.408485,-99.077111)).strokeColor(Color.RED).strokeWidth(2.5f).clickable(true));


        //Polygon de Agricola Pantitlan
        Polygon polygonAP = map.addPolygon(new PolygonOptions()
                .add(new LatLng(19.422144,-99.0555782),
                        new LatLng(19.422230,-99.056035),
                        new LatLng(19.421943,-99.056217),
                        new LatLng(19.421724,-99.056379),
                        new LatLng(19.421380,-99.056795),
                        new LatLng(19.421179,-99.057139),
                        new LatLng(19.420893,-99.057909),
                        new LatLng(19.420816,-99.067985),
                        new LatLng(19.420769,-99.068299),
                        new LatLng(19.420530,-99.068997),
                        new LatLng(19.420071,-99.069747),
                        new LatLng(19.419833,-99.069949),
                        new LatLng(19.408806,-99.077110),
                        new LatLng(19.398224,-99.058554),
                        new LatLng(19.398302,-99.058560),
                        new LatLng(19.400741,-99.058265),
                        new LatLng(19.400725,-99.058130),
                        new LatLng(19.421965,-99.055832),
                        new LatLng(19.422144,-99.0555782)).strokeColor(Color.BLUE).strokeWidth(2.5f).clickable(true));
    }

}
