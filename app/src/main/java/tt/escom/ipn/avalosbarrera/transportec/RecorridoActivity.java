package tt.escom.ipn.avalosbarrera.transportec;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by laavalos on 11/18/2018.
 */

public class RecorridoActivity extends AppCompatActivity implements OnMapReadyCallback{

    GoogleMap mMap;
    LatLng origen, destino;
    int idRuta;
    double lat, lng;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorrido);

        Intent i = getIntent();
        origen = new LatLng(i.getDoubleExtra("latitudOrigen", 0.0), i.getDoubleExtra("longitudOrigen", 0.0));
        destino = new LatLng(i.getDoubleExtra("latitudDestino", 0.0), i.getDoubleExtra("longitudDestino", 0.0));
        idRuta = i.getIntExtra("idRuta", 1);


        try {
            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.fragmentRecorrido);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

    }

    private void inicioRecorrido(final LatLng origen, final LatLng destino, int idRuta) throws JSONException {

        JSONObject puntos = new JSONObject();
        String url = "https://api-transportec.herokuapp.com/public/obtener_punto_cercano_ruta";
        puntos.put("latitudOrigen", origen.latitude);
        puntos.put("longitudOrigen", origen.longitude);
        puntos.put("latitudDestino", destino.latitude);
        puntos.put("longitudDestino", destino.longitude);
        puntos.put("idRuta", idRuta);

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, puntos, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject nearOrigen =  response.optJSONArray("origen").optJSONObject(0);
                JSONObject nearDestino = response.optJSONArray("destino").optJSONObject(0);
                int num_puntoOrigen = nearOrigen.optInt("num_punto");
                int num_puntoDestino = nearDestino.optInt("num_punto");
                LatLng pointNearOrigen = new LatLng(nearOrigen.optDouble("latitud"), nearOrigen.optDouble("longitud"));
                LatLng pointNearDestino = new LatLng(nearDestino.optDouble("latitud"), nearDestino.optDouble("longitud"));

                Polyline route = mMap.addPolyline(new PolylineOptions().add(
                        origen, pointNearOrigen, pointNearDestino, destino).width(9f).color(Color.CYAN));
                CameraUpdate ubicacion = CameraUpdateFactory.newLatLngZoom(origen, 15);
                mMap.animateCamera(ubicacion);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);


    }

    private void actualizarRecorrido(){

    }

    private void finRecorrido(){

    }

    private void miUbicacion() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        actualizarUbicacion(location);
    }

    private void actualizarUbicacion(Location location) {
        if (location != null) {
            lat = location.getLatitude();
            lng = location.getLongitude();
        }

    }

    LocationListener locListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            actualizarUbicacion(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

}
