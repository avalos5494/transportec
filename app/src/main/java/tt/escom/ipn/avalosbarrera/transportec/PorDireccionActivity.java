package tt.escom.ipn.avalosbarrera.transportec;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by laavalos on 10/29/2018.
 */

public class PorDireccionActivity extends AppCompatActivity {

    boolean isOrigen, isModificar;
    LatLng lg;
    double latOrigen, lngOrigen;
    double latDestino, lngDestino;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_por_direccion);
        findViewById(R.id.button10).setVisibility(View.INVISIBLE);
        Intent i = getIntent();
        isOrigen = i.getBooleanExtra("isOrigen", true);
        latOrigen = i.getDoubleExtra("latitudOrigen", 0.0);
        lngOrigen = i.getDoubleExtra("longitudOrigen", 0.0);
        latDestino = i.getDoubleExtra("latitudDestino", 0.0);
        lngDestino = i.getDoubleExtra("longitudDestino", 0.0);
        isModificar = i.getBooleanExtra("isModificar", false);
         //Limita el autocompletado de la busqueda en maps para mostrar solo resultados de un area especifica
        PlaceAutocompleteFragment paf = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.fragmentAutocomplete);
        //El primer LatLng es la esquina suroeste, el segundo LatLng es la esquina noreste, se usan para formar un rectangulo de area para la busqueda
        paf.setBoundsBias(new LatLngBounds(new LatLng(19.382598, -99.077310), new LatLng(19.422230, -99.0555782)));
        AutocompleteFilter acf = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
        paf.setFilter(acf);
        paf.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                 lg = place.getLatLng();
                validarDireccion(lg.latitude, lg.longitude);
            }

            @Override
            public void onError(Status status) {

            }
        });

        accionBotones();

    }

    public void accionBotones(){
        Button aceptar = (Button) findViewById(R.id.button10);
        Button atras = (Button) findViewById(R.id.button9);

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isModificar) {
                    if (isOrigen) {
                        latOrigen = lg.latitude;
                        lngOrigen = lg.longitude;
                        final Intent intent = new Intent(PorDireccionActivity.this, DestinoActivity.class);
                        intent.putExtra("latitudOrigen", latOrigen);
                        intent.putExtra("longitudOrigen", lngOrigen);
                        AlertDialog alerta = new AlertDialog.Builder(PorDireccionActivity.this).create();
                        alerta.setTitle("Confirmación");
                        alerta.setMessage("Desea seleccionar esta ubicacion como Origen");
                        alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                startActivity(intent);
                            }
                        });
                        alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alerta.show();
                    } else {
                        latDestino = lg.latitude;
                        lngDestino = lg.longitude;
                        final Intent intent = new Intent(PorDireccionActivity.this, ElegirRutaActivity.class);
                        intent.putExtra("latitudOrigen", latOrigen);
                        intent.putExtra("longitudOrigen", lngOrigen);
                        intent.putExtra("latitudDestino", latDestino);
                        intent.putExtra("longitudDestino", lngDestino);
                        AlertDialog alerta = new AlertDialog.Builder(PorDireccionActivity.this).create();
                        alerta.setTitle("Confirmación");
                        alerta.setMessage("Desea seleccionar esta ubicacion como Destino");
                        alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                startActivity(intent);
                            }
                        });
                        alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alerta.show();

                    }
                }
                else{
                    if(isOrigen){
                        latOrigen = lg.latitude;
                        lngOrigen = lg.longitude;
                        final Intent intent = new Intent(PorDireccionActivity.this, ElegirRutaActivity.class);
                        intent.putExtra("latitudOrigen", latOrigen);
                        intent.putExtra("longitudOrigen", lngOrigen);
                        intent.putExtra("latitudDestino", latDestino);
                        intent.putExtra("longitudDestino", lngDestino);
                        AlertDialog alerta = new AlertDialog.Builder(PorDireccionActivity.this).create();
                        alerta.setTitle("Confirmación");
                        alerta.setMessage("Desea seleccionar esta ubicacion como Origen");
                        alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                startActivity(intent);
                            }
                        });
                        alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alerta.show();
                    }
                    else {
                        latDestino = lg.latitude;
                        lngDestino = lg.longitude;
                        final Intent intent = new Intent(PorDireccionActivity.this, ElegirRutaActivity.class);
                        intent.putExtra("latitudOrigen", latOrigen);
                        intent.putExtra("longitudOrigen", lngOrigen);
                        intent.putExtra("latitudDestino", latDestino);
                        intent.putExtra("longitudDestino", lngDestino);
                        AlertDialog alerta = new AlertDialog.Builder(PorDireccionActivity.this).create();
                        alerta.setTitle("Confirmación");
                        alerta.setMessage("Desea seleccionar esta ubicacion como Destino");
                        alerta.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                startActivity(intent);
                            }
                        });
                        alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alerta.show();
                    }
                }
            }
        });

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private boolean validarDireccion(double lat, double lng){
        //new LatLngBounds(new LatLng(19.382598, -99.084022), new LatLng(19.422230, -99.0555782));
        LatLngBounds areaColonias = new LatLngBounds(new LatLng(19.382598, -99.084022), new LatLng(19.422230, -99.0555782));
        LatLng ubicacion = new LatLng(lat, lng);
        boolean b = areaColonias.contains(ubicacion);
        if(!areaColonias.contains(ubicacion)){
            AlertDialog alerta = new AlertDialog.Builder(PorDireccionActivity.this).create();
            alerta.setTitle("Advertencia");
            alerta.setMessage("La Ubicación seleccionada no es válida. Seleccione una ubicación válida");
            alerta.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    findViewById(R.id.button10).setVisibility(View.INVISIBLE);
                    dialogInterface.dismiss();
                }
            });
            alerta.show();

        }
        else{
            findViewById(R.id.button10).setVisibility(View.VISIBLE);
        }
        return b;
    }

}
